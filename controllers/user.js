const data = require('../data.json');
const fs = require('fs');

module.exports = {
    index: (req, res) => {
        return res.status(200).json({
            status: 'success',
            message: 'Get all data successfully!',
            data: data.users
        });
    },
    show: (req, res) => {
        const { userId } = req.params;
        const user = data.users.filter((u) => u.id == userId)[0];
        if(!user) {
            return res.status(400).json({
                status: 'failed',
                message: 'Data not found!',
                data: null
            });
        }

        return res.status(200).json({
            status: 'success',
            message: 'Get detail data successfully!',
            data: user
        });
    },
    create: (req, res) => {
        const { name, email } = req.body;
        const user = {
            id: data.users[data.users.length - 1].id + 1,
            name,
            email
        };
        
        data.users.push(user);

        fs.writeFileSync('./data.json', JSON.stringify(data, null, 2));

        return res.status(201).json({
            status: 'success',
            message: 'Create new data successfully!',
            data: user
        });
    },
    update: (req, res) => {
        const { userId } = req.params;
        const { name, email } = req.body;

        const foundIdx = data.users.findIndex((u) => u.id == userId);
        if(foundIdx < 0) {
            return res.status(404).json({
                status: 'failed',
                message: 'Data not found!',
                data: null
            });
        }

        if(name) data.users[foundIdx].name = name;
        if(email) data.users[foundIdx].email = email;

        fs.writeFileSync('./data.json', JSON.stringify(data, null, 2));

        return res.status(201).json({
            status: 'success',
            message: 'Update data successfully!',
            data: data.users[foundIdx]
        });
    },
    delete: (req, res) => {
        const { userId } = req.params;

        const foundIdx = data.users.findIndex((u) => u.id == userId);

        if(foundIdx < 0) {
            return res.status(404).json({
                status: 'failed',
                message: 'Data not found!',
                data: null
            });
        }

        data.users.splice(foundIdx, 1);

        fs.writeFileSync('./data.json', JSON.stringify(data, null, 2));

        return res.status(200).json({
            status: 'success',
            message: 'Delete data successfully!',
            data: data.users
        });
    },
};