const data = require('../data.json');
const fs = require('fs');

module.exports = {
    index: (req, res) => {
        return res.status(200).json({
            status: 'success',
            message: 'Get all data successfully!',
            data: data.products
        });
    },
    show: (req, res) => {
        const { productId } = req.params;
        const product = data.products.filter((p) => p.id == productId)[0];
        if(!product) {
            return res.status(400).json({
                status: 'failed',
                message: 'Data not found!',
                data: null
            });
        }

        return res.status(200).json({
            status: 'success',
            message: 'Get detail data successfully!',
            data: product
        });
    },
    create: (req, res) => {
        const { name, description, price } = req.body;
        const product = {
            id: data.products[data.products.length - 1].id + 1,
            name,
            description,
            price
        };

        data.products.push(product);

        fs.writeFileSync('./data.json', JSON.stringify(data, null, 2));

        return res.status(201).json({
            status: 'success',
            message: 'Create new data successfully!',
            data: product
        });
    },
    update: (req, res) => {
        const { productId } = req.params;
        const { name, description, price } = req.body;

        const foundIdx = data.products.findIndex((p) => p.id == productId);
        if(foundIdx < 0) {
            return res.status(404).json({
                status: 'failed',
                message: 'Data not found!',
                data: null
            });
        }

        if(name) data.products[foundIdx].name = name;
        if(description) data.products[foundIdx].description = description;
        if(price) data.products[foundIdx].price = price;

        fs.writeFileSync('./data.json', JSON.stringify(data, null, 2));

        return res.status(201).json({
            status: 'success',
            message: 'Update data successfully!',
            data: data.products[foundIdx]
        });
    },
    delete: (req, res) => {
        const { productId } = req.params;

        const foundIdx = data.products.findIndex((p) => p.id == productId);

        if(foundIdx < 0) {
            return res.status(404).json({
                status: 'failed',
                message: 'Data not found!',
                data: null
            });
        }

        data.products.splice(foundIdx, 1);

        fs.writeFileSync('./data.json', JSON.stringify(data, null, 2));

        return res.status(200).json({
            status: 'success',
            message: 'Delete data successfully!',
            data: data.products
        });
    }
}